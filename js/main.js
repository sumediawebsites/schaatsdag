jQuery.extend(jQuery.validator.messages, {
	required: "Dit is een verplicht veld.",
	remote: "Controleer dit veld.",
	email: "Vul hier een geldig e-mailadres in.",
	url: "Vul hier een geldige URL in.",
	date: "Vul hier een geldige datum in.",
	dateISO: "Vul hier een geldige datum in (ISO-formaat).",
	number: "Vul hier een geldig getal in.",
	digits: "Vul hier alleen getallen in.",
	creditcard: "Vul hier een geldig creditcardnummer in.",
	equalTo: "Vul hier dezelfde waarde in.",
	accept: "Vul hier een waarde in met een geldige extensie.",
	maxlength: jQuery.validator.format("Vul hier maximaal {0} tekens in."),
	minlength: jQuery.validator.format("Vul hier minimaal {0} tekens in."),
	rangelength: jQuery.validator.format("Vul hier een waarde in van minimaal {0} en maximaal {1} tekens."),
	range: jQuery.validator.format("Vul hier een waarde in van minimaal {0} en maximaal {1}."),
	max: jQuery.validator.format("Vul hier een waarde in kleiner dan of gelijk aan {0}."),
	min: jQuery.validator.format("Vul hier een waarde in groter dan of gelijk aan {0}.")
});
$("form#formeen").validate({
  errorElement: "div",
  ignore:":hidden"
});

$('#staptweebutton').click(function(e){
//	e.preventDefault();
	return

//	if($('form#formeen').valid()){
//		$(this).tab('show');
//	}else{
//	}
})

$('#stapeenbutton').click(function(e){
	return

//	e.preventDefault();
//	$(this).tab('show');
})

$('.next-step').click(function(){
	return
//	$('#staptweebutton').click();
})

highestbox = 0;

$('.same-height>div').each(function(){
	if ($(this).outerHeight() > highestbox) {
		highestbox = $(this).outerHeight();
	}
	$('.same-height>div').outerHeight(highestbox);
});

highestbox = 0;

$('.hartstocht>div').each(function(){
	if ($(this).outerHeight() > highestbox) {
		highestbox = $(this).outerHeight();
	}
});
setTimeout(function(){
	$('.hartstocht>div').outerHeight(highestbox);
}, 50);

jQuery(function( $ ){
  $.fn.actualHeight = function(){
        // find the closest visible parent and get it's hidden children
    var visibleParent = this.closest(':visible').children(),
        thisHeight;

    // set a temporary class on the hidden parent of the element
    visibleParent.addClass('temp-show');

    // get the height
    thisHeight = this.height();

    // remove the temporary class
    visibleParent.removeClass('temp-show');

    return thisHeight;
  };
  $('.verplicht-note').each(function(){
	  height = $(this).actualHeight();
	  $(this).css('margin-top', -height - 15);
  })

});
