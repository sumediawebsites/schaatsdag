	<footer>
		<div class="container-fluid  bgc-b links">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-2 col-md-offset-1"><a class="tc-w" href="<?=$_url['actievoorwaarden']?>" target="_blank">Actievoorwaarden</a></div>
						<div class="col-xs-12 col-sm-6 col-md-2"><a class="tc-w" href="<?=$_url['privacy']?>" target="_blank">Privacy</a></div>
						<div class="col-xs-12 col-sm-6 col-md-2"><a class="tc-w" href="<?=$_url['disclaimer']?>" target="_blank">Disclaimer</a></div>
						<div class="col-xs-12 col-sm-6 col-md-2"><a class="tc-w" href="<?=$_url['faq']?>" target="_blank">FAQ</a></div>
						<div class="col-xs-12 col-sm-6 col-md-2"><a class="tc-w" href="https://lotto.nl" target="_blank">Lotto.nl</a></div>

					</div>
				</div>
			</div>
		</div>
		<div class="container social-contact">
			<div class="col-xs-12 col-md-10 footer-left">
				<div class="row">
					<a href="<?=$_url['facebook']?>" target="_blank"><i class="tc-w fa fa-facebook"></i></a>
					<a href="<?=$_url['twitter']?>" target="_blank"><i class="tc-w fa fa-twitter"></i></a>
					<a href="" class="phone"><i class="fa fa-phone tc-w"></i></a><p class="mobile-block"> Kom je er niet uit? Bel voor informatie <b><?=$_txt['telefoon_helpdesk']?></b></p>
					<p class="vergunning">Voor het organiseren van Lotto en Cijferspel is aan De Lotto vergunning verleend door de Kansspelautoriteit.</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-2 footer-right">
				<div class="row">
					<a href="http://weetwatjespeelt.nl" target="_blank">
						<img class="pull-right" src="img/speelbewust.jpg" alt="speel bewust 18+" />
					</a>
					<a href="http://sumedia.nl/" class="sumedia" target="_blank">Sumedia</a>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</footer>
